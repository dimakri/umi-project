import sys
import os
import logging
from dotenv import load_dotenv
from github import Github, GithubException
from datetime import datetime
from elasticsearch import Elasticsearch, exceptions as es_exceptions


load_dotenv()

GITHUB_ACCESS_TOKEN = os.getenv("GITHUB_ACCESS_TOKEN")
ELS_URL = os.getenv("ELS_URL")
GITHUB_REPO = os.getenv("GITHUB_REPO")
ELS_INDEX_NAME = os.getenv("ELS_INDEX_NAME")

log_format = "%(asctime)s %(levelname)s %(message)s"
logging.basicConfig(level="INFO", format=log_format, datefmt="%Y-%m-%d %H:%M:%S")


def main():
    logging.info("Connecting to the elasticsearch instance...")
    es = Elasticsearch([ELS_URL])
    logging.info("Connecting to Github using provided token...")
    github = Github(GITHUB_ACCESS_TOKEN)

    try:
        commits = github.get_repo(GITHUB_REPO).get_commits()
        logging.info(
            f"Github repository {GITHUB_REPO} has total of {commits.totalCount} commits."
        )
    except GithubException:
        logging.error("Check if you Github Authtoken is correct!")
        sys.exit()

    for idx, commit in enumerate(commits):
        if commit.author is None:
            # if the github user has been deleted, we cannon acquire
            # the creation so I am setting it to 00:00:00 UTC on 1 January 1970
            user_created_at = datetime.fromtimestamp(0)
        else:
            user_created_at = commit.author.created_at
        create_es_doc(
            commit.commit.author.date,
            commit.commit.author.name,
            commit.commit.message,
            user_created_at,
            commit.sha,
            es,
        )
    logging.info(
        f"{idx+1} of {commits.totalCount} added to Elasticsearch {ELS_INDEX_NAME} index."
    )


def create_es_doc(
    commit_date: datetime,
    author: str,
    message: str,
    use_created_at: datetime,
    commit_sha: str,
    els_connection,
):

    doc = {
        "author": author,
        "message": message,
        "commit_date": commit_date,
        "use_created_at": use_created_at,
    }
    try:
        res = els_connection.index(index=ELS_INDEX_NAME, id=commit_sha, body=doc)
    except es_exceptions.RequestError:
        logging.error(
            "Not able to connect to the Elasticsearch instance. Check your ELS url."
        )
        sys.exit()


if __name__ == "__main__":
    main()
