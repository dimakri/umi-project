
<br />
<p align="center">
  <h3 align="center">README</h3>

  <p align="center">
    UMI take-home exercise
  </p>
</p>

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#directory-structure">Directory Structure</a></li>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#usage">Usage</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>


## About The Project

### The assigment
For an inexplicable reason, our product owner really wants all commit activity from this public repo to be available as separate documents in Elastic.They further specified that each document should include: the date of each commit the username of the committer the commit message.Furthermore, they want the indexed documents to also include the date the committer created his account on GitHub.

### Directory Structure
   ```sh
    .
    ├── README.md
    ├── main.py
    └── requirements.txt
   ```
## Getting Started
### Prerequisites

To run this project locally, python3 (I am using the latest Python 3.9.6) need to be installed on you machine, also your Elasticsearch instance has to be running.

### Usage


1. Install all of the requirements:

   ```sh
   pip3 install -r ./requirements.txt
   ```

2. Set environmet variables in .env file:

    ```sh
    GITHUB_ACCESS_TOKEN = "XXXXXXXX"
    ELS_URL = "XXXXXXXX"    #should be formated as https://user:secret@localhost:443
    GITHUB_REPO = "RockstarLang/rockstar"
    ELS_INDEX_NAME = "umi"
    ```

2. Execute the main.py program with some of the example variables:

   ```sh
   python3 main.py
   ```

   Output:

   ```sh
    2021-08-04 12:02:30     Connecting to the elasticsearch instance...
    2021-08-04 12:02:30     Connecting to Github using provided token...
    2021-08-04 12:02:31     Github repository RockstarLang/rockstar has total of 316 commits.
    2021-08-04 12:02:31     PUT xxxxxxxxxxxx [status:200 request:0.288s]
    2021-08-04 12:02:31     PUT xxxxxxxxxxxx [status:200 request:0.034s]
    2021-08-04 12:02:32     PUT xxxxxxxxxxxx [status:200 request:0.131s]
    2021-08-04 12:02:32     PUT xxxxxxxxxxxx [status:200 request:0.037s]
    316 of 316 added to Elasticsearch umi index
    ```
## Contact

Dmitrii Krivosheev - dkrivosheyev@gmail.com

